package itmatic.io.bloodshareapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class VarifyMeActivity extends AppCompatActivity {

    private Button confirmOtpButton;
    private EditText FirstOtpNumberEdittext, SecondOtpNumberEdittext, ThirdOtpNumberEdittext, FourthOtpNumberEdittext;
    private TextView varifyOtpTextview, resendOtpTextview, otpsendTextview, ResendOtpTextview, LogoutTextview;
    private ProgressDialog dialog;
    private SharedPreferences.Editor editor;
    private TextInputLayout textInputLayout;
    private TextView OtpnotReceivedTextview;
    private  String PhoneNuumber,password;
    private LinearLayout ParentLinearLayout;
    private  boolean bitforotp=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varify_me);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.RED));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.red));
        }
        FirstOtpNumberEdittext = (EditText) findViewById(R.id.et_first_otp_number);
        SecondOtpNumberEdittext = (EditText) findViewById(R.id.et_second_otp_number);
        ThirdOtpNumberEdittext = (EditText) findViewById(R.id.et_third_otp_number);
        FourthOtpNumberEdittext = (EditText) findViewById(R.id.et_fourth_otp_number);
        OtpnotReceivedTextview = (TextView) findViewById(R.id.tv_did_not_receive_otp);
        ResendOtpTextview = (TextView) findViewById(R.id.tv_resend_otp);
        confirmOtpButton = (Button) findViewById(R.id.bt_confirm_otp);
        LogoutTextview = (TextView) findViewById(R.id.tv_logout);
        ParentLinearLayout=(LinearLayout)findViewById(R.id.ll_parent_otp_layout);

        Intent phonenumberintent= getIntent();

        if(phonenumberintent!=null)
        {
            PhoneNuumber= phonenumberintent.getStringExtra("phonenumber");
            password=phonenumberintent.getStringExtra("password");
        }

        ResendOtpTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Token();*/
            }
        });
        // textInputLayout=(TextInputLayout)findViewById(R.id.til_check_otp_all_value);
        LogoutTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

          /*      SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
                sharedPreferences.edit().remove("token").commit();

                sharedPreferences.edit().remove("fullname").commit();
                sharedPreferences.edit().remove("email_address").commit();*/
                editor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                editor.putBoolean("OTP", false);
                editor.commit();
                bitforotp=false;
                Intent myIntent = new Intent(VarifyMeActivity.this,
                        LoginActivity.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(myIntent);
            }
        });

        FirstOtpNumberEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    SecondOtpNumberEdittext.requestFocus();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        SecondOtpNumberEdittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(SecondOtpNumberEdittext.getText().length() == 0)
                    FirstOtpNumberEdittext.requestFocus();


                return false;
            }
        });
        SecondOtpNumberEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    ThirdOtpNumberEdittext.requestFocus();

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ThirdOtpNumberEdittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(ThirdOtpNumberEdittext.getText().length() == 0)
                    SecondOtpNumberEdittext.requestFocus();


                return false;
            }
        });

        ThirdOtpNumberEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    FourthOtpNumberEdittext.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        FourthOtpNumberEdittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(FourthOtpNumberEdittext.getText().length() == 0)
                    ThirdOtpNumberEdittext.requestFocus();


                return false;
            }
        });


        FourthOtpNumberEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 1) {
                    if (FirstOtpNumberEdittext.getText().toString().equals("")
                            )
                        Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();

                    else if (SecondOtpNumberEdittext.getText().toString().equals(""))

                    {
                        Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();
                    } else if (ThirdOtpNumberEdittext.getText().toString().equals("")) {
                        Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();
                    } else if (FourthOtpNumberEdittext.getText().toString().equals("")) {
                        Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();
                    } else {
                   /*     VarifyOtp();*/
                    }



                }

                if(charSequence.length()==0)
                {
                    ThirdOtpNumberEdittext.requestFocus();
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        confirmOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (FirstOtpNumberEdittext.getText().toString().equals("")
                        )
                    Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();

                else if (SecondOtpNumberEdittext.getText().toString().equals(""))

                {
                    Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();
                } else if (ThirdOtpNumberEdittext.getText().toString().equals("")) {
                    Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();
                } else if (FourthOtpNumberEdittext.getText().toString().equals("")) {
                    Toast.makeText(VarifyMeActivity.this, "Enter OTP Code", Toast.LENGTH_SHORT).show();
                } else {
                   /* VarifyOtp();*/
                }


            }
        });


        OtpnotReceivedTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }


    public void onStop() {
        super.onStop();
        if(bitforotp)
        {
            editor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
            editor.putBoolean("OTP", true);
            editor.commit();
        }

    }


    String checkvaliddata() {


        if (!FirstOtpNumberEdittext.getText().toString().equals("") && !SecondOtpNumberEdittext.getText().toString().equals("") && !ThirdOtpNumberEdittext.getText().toString().equals("") && !FourthOtpNumberEdittext.getText().toString().equals("")) {

            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(this.getString(R.string.invalide_otp_message));

            return "Please Provide Email address";
        }


        return "success";
    }
/*
    public void VarifyOtp() {
        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue(); // get a object from  vollysingleton class
        dialog = new LoginActivity().ShowConstantProgressNOTCAN(this, this.getString(R.string.varifying_otp_in_dialog_message), this.getString(R.string.login_signin_pleasewait));
        dialog.show();

        String url = ("/api/v1/user/verify-otp.json");

        StringRequest arrivedrequest = new StringRequest(Request.Method.POST, Resource.BASE_URL + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject user = new JSONObject(response);  // response is a array come from API
                    boolean status = user.getBoolean("success");

                    //  JSONObject user=data.getJSONObject("response");

                    // success is a status come from API
                    if (status) {
                        JSONObject userdetail = user.getJSONObject("response");
                        UserDetail userDetail = new UserDetail();

                        userDetail.setId(userdetail.getInt("id"));
                        userDetail.setEmail(userdetail.getString("email"));
                        userDetail.setPhonenumber(userdetail.getString("phone"));
                        userDetail.setShorturl(userdetail.getString("short_url"));
                        userDetail.setCity(userdetail.getString("city"));
                        userDetail.setAccesstoken(userdetail.getString("access_token"));
                        userDetail.setFullname(userdetail.getString("full_name"));
                        Resource.sToken = userdetail.getString("access_token");
                        editor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                        editor.putString("token", userdetail.getString("access_token"));
                        editor.putString("fullname", userdetail.getString("full_name"));
                        editor.putString("email_address", userdetail.getString("email"));
                        editor.commit();

                        Intent myIntent = new Intent(VarifyOtp.this,
                                MainActivity.class);
                        startActivity(myIntent);
                        finish();

                    } else {

                        dialog.dismiss();
                        JSONObject userdetaillist = user.getJSONObject("error");
                        String error = userdetaillist.getString("description");
                        new LoginActivity().showerror(error, VarifyOtp.this);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.getStackTrace();

                if (error instanceof NoConnectionError) {

                    Snackbar snackbar = Snackbar
                            .make(ParentLinearLayout, "Internet issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Token();
                                }
                            });

                    snackbar.show();
                }

            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                String code = FirstOtpNumberEdittext.getText().toString() + SecondOtpNumberEdittext.getText().toString() + ThirdOtpNumberEdittext.getText().toString() + FourthOtpNumberEdittext.getText().toString();
                params.put("otp", code);

                return params;
            }


        };


        arrivedrequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(arrivedrequest);


    }
    public void Token() {
        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue(); // get a object from  vollysingleton class
        dialog = new LoginActivity().ShowConstantProgressNOTCAN(this, this.getString(R.string.resending_otp_dialog_message), this.getString(R.string.login_signin_pleasewait));
        dialog.show();

        String url = ("/api/v1/user/login.json");

        StringRequest arrivedrequest = new StringRequest(Request.Method.POST, Resource.BASE_URL + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject data = new JSONObject(response);  // response is a array come from API
                    boolean status = data.getBoolean("success");// success is a status come from API
                    if (status) {

                        Toast.makeText(VarifyOtp.this,"Otp resend successfully",Toast.LENGTH_SHORT).show();

                    } else {

                        dialog.dismiss();
                        JSONObject userdetaillist = data.getJSONObject("error");
                        String error = userdetaillist.getString("description");
                        new LoginActivity(). showerror(error, VarifyOtp.this);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.getStackTrace();

                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar
                            .make(ParentLinearLayout, "Internet issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Token();
                                }
                            });

                    snackbar.show();

                }

            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email_or_phone", PhoneNuumber);
                params.put("password", password);
                return params;
            }


        };


        arrivedrequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(arrivedrequest);


    }*/
}
